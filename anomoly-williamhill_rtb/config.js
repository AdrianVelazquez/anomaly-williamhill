'use strict'

/**
 * Use this config file to customize settings for how
 * The Banner Creator builds the templates. All settings
 * in this config are also set by the creator itself,
 * meaning that you can leave this file blank or even remove
 * it if you don't need any customizations.
 */

module.exports = {
	// Location of where you develop your formats
	// dev_dir: 'dev',

  // Location for the creator to place built formats
	// build_dir: 'build',

  // Location for the creator to place release builds
  // Not currently used!
	// deploy_dir: 'delivery',

  // Location of manually generated fallbacks
  // This is where you put your fallbacks if you use the chrome plugin
  // or the Flink-made-fallback-generator
	// fallback_dir: 'fallback',

	// Default clicktag to be used on platforms that uses
	// a hard coded fallback (DCM/Adform)
	// Can be set to an empty string ''
	// default_clicktag: 'https://www.example.com',

  // Are manual fallbacks created after build or before?
  // (Are fallbacks named after dev names or build names)
  // fb_after_build: true,

  // Regular CDN for TweenMax
  // Change this to use a different version from either cdn, or from the working folder
	// GSAP: 'https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.4/TweenMax.min.js',

  // Google CDN for TweenMax
  // Google uses a different CDN, if you are building DCS or DCM, change here
	// GSAP_GOOGLE: 'https://s0.2mdn.net/ads/studio/cached_libs/tweenmax_1.18.0_499ba64a23378545748ff12d372e59e9_min.js',


		/**
		 * This function returns the name to be used on formats that are built
		 * Use this to specify a new name on everything that is built
		 *
		 * IMPORTAN! Make sure to always check for variant, it's not forced so no way of knowing if it is used
		 *
		 * @property {string}  obj             - The parsed dev name of the banner
		 * @property {string}  obj.client      - The client/brand specified in the dev name
		 * @property {string}  obj.project     - The project name specified in the dev name
		 * @property {string}  obj.width       - The width specified in the dev name
		 * @property {string}  obj.height      - The height specified in the dev name
		 * @property {string}  obj.execution   - The (optional) execution specified in the dev name
		 * @property {string}  obj.variant     - The (optional) variant specified in the dev name
		 */
    // buildName: function(obj) {
		// 	if (obj.execution) {
		//   	// Edit names for formats with executions like this:
		// 		return obj.client + '_' + obj.project + '_' + obj.execution + '_' + obj.width  + 'x' + obj.height + (obj.variant ? '_' + obj.variant : '');
		// 	} else {
    // 		// Edit names for formats without executions like this:
		// 		return obj.client + '_' + obj.project + '_' + obj.width  + 'x' + obj.height + (obj.variant ? '_' + obj.variant : '');
		// 	}
		// }
};
