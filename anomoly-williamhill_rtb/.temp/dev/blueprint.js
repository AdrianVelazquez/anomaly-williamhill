var bannerboy = bannerboy || {};
bannerboy.addToBlueprint({
	"elements": {
		"cta": {
			"left": 65,
			"top": 206,
			"backgroundImage": "cta.png",
			"retina": true,
			"id": "cta",
			"parent": "banner"
		},
		"frame_1_txt": {
			"left": 59,
			"top": 81,
			"width": 185,
			"height": 90,
			"id": "frame_1_txt",
			"parent": "banner"
		},
		"frame_1_txt_1": {
			"left": 24,
			"top": 0,
			"backgroundImage": "frame_1_txt_1.png",
			"retina": true,
			"id": "frame_1_txt_1",
			"parent": "frame_1_txt"
		},
		"frame_1_txt_2": {
			"left": 0,
			"top": 20,
			"backgroundImage": "frame_1_txt_2.png",
			"retina": true,
			"id": "frame_1_txt_2",
			"parent": "frame_1_txt"
		},
		"frame_1_txt_3": {
			"left": 2,
			"top": 64,
			"backgroundImage": "frame_1_txt_3.png",
			"retina": true,
			"id": "frame_1_txt_3",
			"parent": "frame_1_txt"
		},
		"frame_2_txt": {
			"left": 67,
			"top": 81,
			"width": 159,
			"height": 103,
			"id": "frame_2_txt",
			"parent": "banner"
		},
		"frame_2_txt_3": {
			"left": 7,
			"top": 64,
			"backgroundImage": "frame_2_txt_3.png",
			"retina": true,
			"id": "frame_2_txt_3",
			"parent": "frame_2_txt"
		},
		"frame_2_txt_2": {
			"left": 0,
			"top": 32,
			"backgroundImage": "frame_2_txt_2.png",
			"retina": true,
			"id": "frame_2_txt_2",
			"parent": "frame_2_txt"
		},
		"frame_2_txt_1": {
			"left": 22,
			"top": 0,
			"backgroundImage": "frame_2_txt_1.png",
			"retina": true,
			"id": "frame_2_txt_1",
			"parent": "frame_2_txt"
		},
		"frame_2_txt_4": {
			"left": 36,
			"top": 52,
			"backgroundImage": "frame_2_txt_4.png",
			"retina": true,
			"id": "frame_2_txt_4",
			"parent": "frame_2_txt"
		},
		"frame_3_txt": {
			"left": 55,
			"top": 82,
			"width": 191,
			"height": 66,
			"id": "frame_3_txt",
			"parent": "banner"
		},
		"frame_3_txt_2": {
			"left": 0,
			"top": 32,
			"backgroundImage": "frame_3_txt_2.png",
			"retina": true,
			"id": "frame_3_txt_2",
			"parent": "frame_3_txt"
		},
		"frame_3_txt_1": {
			"left": 36,
			"top": 0,
			"backgroundImage": "frame_3_txt_1.png",
			"retina": true,
			"id": "frame_3_txt_1",
			"parent": "frame_3_txt"
		},
		"frame_3_txt_3": {
			"left": 43,
			"top": 20,
			"backgroundImage": "frame_3_txt_3.png",
			"retina": true,
			"id": "frame_3_txt_3",
			"parent": "frame_3_txt"
		},
		"end_frame": {
			"left": 5,
			"top": 52,
			"width": 290,
			"height": 193,
			"id": "end_frame",
			"parent": "banner"
		},
		"end_frame_disclaimer": {
			"left": 0,
			"top": 166,
			"backgroundImage": "end_frame_disclaimer.png",
			"retina": true,
			"id": "end_frame_disclaimer",
			"parent": "end_frame"
		},
		"get_apple": {
			"left": 155,
			"top": 120,
			"backgroundImage": "get_apple.png",
			"retina": true,
			"id": "get_apple",
			"parent": "end_frame"
		},
		"get_android": {
			"left": 39,
			"top": 120,
			"backgroundImage": "get_android.png",
			"retina": true,
			"id": "get_android",
			"parent": "end_frame"
		},
		"logo": {
			"left": 21,
			"top": 0,
			"backgroundImage": "logo.png",
			"retina": true,
			"id": "logo",
			"parent": "end_frame"
		}
	},
	"images": [
		"cta.png",
		"frame_1_txt_1.png",
		"frame_1_txt_2.png",
		"frame_1_txt_3.png",
		"frame_2_txt_3.png",
		"frame_2_txt_2.png",
		"frame_2_txt_1.png",
		"frame_2_txt_4.png",
		"frame_3_txt_2.png",
		"frame_3_txt_1.png",
		"frame_3_txt_3.png",
		"end_frame_disclaimer.png",
		"get_apple.png",
		"get_android.png",
		"logo.png"
	],
	"width": 300,
	"height": 250,
	"executions": []
});