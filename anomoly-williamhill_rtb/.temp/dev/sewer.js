var bannerboy = bannerboy || {};

bannerboy.main = function() {
	bannerboy.preloadImages(bannerboy.blueprint.images, bannerboy.onReady);
};

bannerboy.clickthrough = function() {
	window.open(window.clickTag, '_blank');
};
