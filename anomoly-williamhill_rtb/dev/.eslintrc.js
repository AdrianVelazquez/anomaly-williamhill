module.exports = {
    "env": {
        "browser": true,
        "node": true,
        "es6": false,
    },
    "extends": "eslint:recommended",
    "rules": {
        "indent": [
            "warn",
            "tab",
            { "SwitchCase": 1 }
        ],
        "quotes": 0,
        "semi": ["warn", "always"],
        "no-console": 0,
        "comma-spacing": [
            "warn",
            {
                "before" : false,
                "after": true
            }
        ],
        "space-infix-ops": ["warn"],
        "no-unused-vars": ["warn"],
        "no-empty": ["warn"],
        "no-constant-condition": 0,
    },

    "globals": {

        // Bannerboy
        "BBTimeline": true,
        "bannerboy": true,
        "bb": true,

        // Greensock
        "TweenLite": true,
        "TweenMax": true,
        "TimelineLite": true,
        "TimelineMax": true,
        "CSSPlugin": true,
        "Linear": true,
        "Power0": true,
        "Power1": true,
        "Power2": true,
        "Power3": true,
        "Power4": true,
        "Back": true,
        "Sine": true,
        "Elastic": true,
        "Draggable": true,
        "Expo": true,

        // DC
        "Enabler": true,
        "studioinnovation": true,
        "studio": true,

        // Adform
        "dhtml": true,
        "Adform": true,

        // Sizmek
        "EB": true,
        "EBG": true,

        // Other
        "PIXI": true,
        "Stats": true,
        "THREE": true,
        "$": true,
        "screenad": true,
        "YT": true,
        "createjs": true,
        "PerspectiveTransform": true,
    }
};
