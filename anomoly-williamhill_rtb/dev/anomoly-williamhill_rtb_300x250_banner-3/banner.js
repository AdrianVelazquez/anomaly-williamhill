var bannerboy = bannerboy || {};

bannerboy.addToBlueprint({images: []});

bannerboy.onReady = function() {

	/* Init
	================================================= */

	var width = bannerboy.blueprint.width;
	var height = bannerboy.blueprint.height;
	var banner = bannerboy.createElement({id: "banner",	width: width, height: height, backgroundColor: "#0a174e", overflow: "hidden", cursor: "pointer", parent: 'banner-container'});
	var border = bannerboy.createElement({border: "solid 1px #000", boxSizing: "border-box", width: width, height: height, parent: 'banner-container'});
	var bb = bannerboy.createElementsFromBlueprint();

	/* Adjustments
	================================================= */

	/* Animations
	================================================= */
	var txt_in_vertical = function(element, direction) {
		return new BBTimeline()
			.fromTo(element, 0.18, {scale: 3, rotation: 5 * direction, opacity: 0}, {scale: 0.97, rotation: 0, opacity: 1, ease: Power2.easeIn})
			.offset(0.22)
			.add(shakeCameraAnim())
			.offset(0.02)
			.to(element, 0.08, {scale: 1.02, ease: Power2.easeOut})
			.chain()
			.to(element, 0.08, {scale: 1, ease: Power2.easeOut});
	};

	var txt_out_vertical = function(element, direction) {
		return new BBTimeline()
			.to(element, 0.25, {opacity: 0, y: 25 * direction, ease: Power2.easeIn});
	};

	var txt_in_horizontal = function(element, direction) {
		return new BBTimeline()
			.from(element, 0.75, {opacity: 0, x: 25 * direction, ease: Power3.easeOut});
	};

	var txt_out_horizontal = function(element, direction) {
		return new BBTimeline()
			.to(element, 0.25, {opacity: 0, x: 25 * direction, ease: Power2.easeIn});
	};

	var accent_txt_in = function(element) {
		return new BBTimeline()
			.from(element, 0.5, {scale: 0, ease: Back.easeOut.config(1.4)});
	};

	var accent_txt_out = function(element) {
		return new BBTimeline()
			.to(element, 0.5, {scale: 0, opacity: 0, ease: Back.easeIn.config(1.4)});
	};

	bb.cta.up = new BBTimeline()
		.to(bb.cta, 0.5, {y: -86});

	bb.logo.in = new BBTimeline()
		.from(bb.logo, 0.5, {opacity: 0, y:-15});

	bb.end_frame_disclaimer.in = new BBTimeline()
		.from(bb.end_frame_disclaimer, 0.5, {opacity: 0});

	/* Main timeline
	================================================= */
	var main_tl = new BBTimeline()
		.offset(0.15)

		// frame 1 in
		.add(txt_in_vertical(bb.frame_1_txt_1, -1))
		.add(txt_in_vertical(bb.frame_1_txt_3, 1))
		.add(accent_txt_in(bb.frame_1_txt_2))
		.chain(2)

		// frame 1 out
		.add(accent_txt_out(bb.frame_1_txt_2))
		.offset(0.35)
		.add(txt_out_vertical(bb.frame_1_txt_1, -1))
		.add(txt_out_vertical(bb.frame_1_txt_3, 1))
		.chain(0.15)

		//frame 2 in
		.add(txt_in_vertical(bb.frame_2_txt_1, -1))
		.add(txt_in_vertical(bb.frame_2_txt_2, 1))
		.add(txt_in_vertical(bb.frame_2_txt_3, -1))
		.add(accent_txt_in(bb.frame_2_txt_4))
		.chain(2)

		//frame 2 out
		.add(accent_txt_out(bb.frame_2_txt_4))
		.offset(0.35)
		.add(txt_out_vertical(bb.frame_2_txt_1, -1))
		.add(txt_out_vertical(bb.frame_2_txt_2, 1))
		.add(txt_out_horizontal(bb.frame_2_txt_3, -1))
		.chain(0.15)

		//frame 3 in
		.add(txt_in_vertical(bb.frame_3_txt_1, -1))
		.add(txt_in_vertical(bb.frame_3_txt_2, -1))
		.add(accent_txt_in(bb.frame_3_txt_3))
		.chain(2)

		//frame 3 out
		.add(accent_txt_out(bb.frame_3_txt_3))
		.offset(0.35)
		.add(txt_out_vertical(bb.frame_3_txt_1, -1))
		.add(txt_out_horizontal(bb.frame_3_txt_2, -1))
		.chain(0.15)

		//end frame in
		.add(bb.logo.in)
		.add(bb.cta.up)
		.offset(0.35)
		.add(txt_in_horizontal(bb.get_apple, -1))
		.add(txt_in_horizontal(bb.get_android, 1))
		.offset(0.35)
		.add(bb.end_frame_disclaimer.in)
		;

	/* Interactions
	================================================= */
	banner.addEventListener("click", function() {
		bannerboy.clickthrough();
	});

	/* Helpers
	================================================= */
	function shakeCameraAnim(settings) {
		var tl = new BBTimeline();

		settings = bannerboy.combineObjects({
			speed: 0.1,
			shakes: 5,
			power: 2,
			multiplier: 0.7
		}, settings);

		for (var i = 0; i < settings.shakes; i++) {
			tl.add(TweenMax.to(banner, settings.speed, {y: (i % 2) ? -settings.power : settings.power, ease: Power1.easeOut})).chain();

			settings.speed *= settings.multiplier;
			settings.power *= settings.multiplier;
		}

		tl.add(TweenMax.to(banner, settings.speed * 2, {y: 0, ease: Power1.easeInOut}));

		return tl;
	}

	/* Scrubber
	================================================= */
	function scrubber(tl) {
		if (window.location.origin == "http://localhost:8080") {
			bannerboy.include(["../bannerboy_scrubber.min.js"], function() {
				if (bannerboy.scrubberController) bannerboy.scrubberController.create({"main timeline": tl});
			});
		}
	}
};
