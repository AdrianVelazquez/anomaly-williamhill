
/* Note: changes in this file will be discarded on build!
=========================================================== */

var bannerboy = bannerboy || {};
bannerboy.execution = ''; // set this to the execution you want to preview

bannerboy.main = function() {
	if(bannerboy.execution) {
		bannerboy.include('material/' + bannerboy.execution + '/blueprint.js', preload);
	} else {
		preload();
	}
};

function preload() {
	bannerboy.preloadImages(bannerboy.blueprint.images, bannerboy.onReady);
}

bannerboy.clickthrough = function() {
	console.log('click');
};